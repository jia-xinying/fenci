import jieba

# 创建停用词列表
def stopwordslist():
    stopwords = [line.strip() for line in open('hit_stopwords.txt', encoding='UTF-8').readlines()]
    return stopwords

# 对句子进行中文分词
def seg_depart(sentence):
    # 对文档中的每一行进行中文分词
    print("正在分词")
    sentence_depart = jieba.cut(sentence.strip())
    # 创建一个停用词列表
    stopwords = stopwordslist()
    # 输出结果为outstr
    outstr = ''
    # 去停用词
    for word in sentence_depart:
        if word not in stopwords:
            if word != '\t':
                outstr += word
                outstr += " "
    return outstr

#统计词频2
def count_num(words):
    counts = {}

    for word in words:
        if len(word) == 1:  # 单个词语不计算在内
            continue
        else:
            counts[word] = counts.get(word, 0) + 1  # 遍历所有词语，每出现一次其对应的值加 1

    items = list(counts.items())  # 将键值对转换成列表
    items.sort(key=lambda x: x[1], reverse=True)  # 根据词语出现的次数进行从大到小排序
    # print(items)

    for n in range(5):#前十五个
        word, count = items[i]
        print("{0:<5}{1:>5}".format(word, count))


# 给出文档路径
filename = "reply.txt"
outfilename = "out_reply.txt"
inputs = open(filename, 'r',encoding='utf-8')
outputs = open(outfilename, 'w',encoding='utf-8')

words = inputs

# 将输出结果写入ou.txt中
for line in inputs:
    line_seg = seg_depart(line)

    outputs.write(line_seg + '\n')
    print("-------------------正在分词和去停用词-----------")
outputs.close()
inputs.close()
print("删除停用词和分词成功！！！")
count_num(i)
